import checkListIcon from "../../assets/checkList.png";
import "./Todo.scss";
import TodoItem from "./TodoItem";
import { useState } from "react";

const Todo = () => {
  const [task, setTask] = useState("");
  const [todos, setTodos] = useState([]);
  const addTask = () => {
    if (task.trim() !== "") {
      const newTodo = {
        id: Math.random(),
        text: task,
        completed: false,
      };
      setTodos([...todos, newTodo]);
      setTask("");
    }
  };

  const handleDeleteTodo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  const handleToggleTodo = (id) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
    );
  };

   const handleEditTodo = (id, newText) => {
     setTodos(
       todos.map((todo) => (todo.id === id ? { ...todo, text: newText } : todo))
     );
   };

  return (
    <main>
      <div className="toDoContainer">
        <div className="headings">
          <h2>To Do List</h2>
          <img src={checkListIcon} alt="To do list icon" />
        </div>
        <div className="input-div">
          <input
            type="text"
            placeholder="Add your task here"
            id="tasks"
            value={task}
            onChange={(e) => setTask(e.target.value)}
          />
          <button onClick={addTask}>Add</button>
        </div>
        <ul className="todo-list">
          {todos.map((todo) => (
            <TodoItem
              className="li"
              key={todo.id}
              todo={todo}
              onDelete={handleDeleteTodo}
              onToggle={handleToggleTodo}
              onEdit={handleEditTodo}
            />
          ))}
        </ul>
      </div>
    </main>
  );
};

export default Todo;
