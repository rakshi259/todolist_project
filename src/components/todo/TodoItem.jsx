import "./TodoItem.scss";
import { useState } from "react";

const TodoItem = ({ todo, onDelete, onToggle, onEdit }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [editText, setEditText] = useState(todo.text);

  const handleEdit = () => {
    onEdit(todo.id, editText);
    setIsEditing(false);
  };
  return (
    <li className={todo.completed ? "completed" : ""}>
      {/* <input
        type="checkbox"
        checked={todo.completed}
        onChange={() => onToggle(todo.id)}
      />
      {todo.text}
      <button onClick={() => onDelete(todo.id)} className="delete">
        Delete
      </button> */}
      {isEditing ? (
        <div className="edit-container">
          <input
            type="text"
            value={editText}
            onChange={(e) => setEditText(e.target.value)}
            autoFocus
            className="input-edit"
          />
          <button onClick={handleEdit} className="save">Save</button>
          <button onClick={() => setIsEditing(false)} className="cancel">Cancel</button>
        </div>
      ) : (
        <div className="list-container">
          <input
            type="checkbox"
            checked={todo.completed}
            onChange={() => onToggle(todo.id)}
          />
          <span>{todo.text}</span>
          <button onClick={() => setIsEditing(true)} className="edit">Edit</button>
          <button onClick={() => onDelete(todo.id)} className="delete">Delete</button>
        </div>
      )}
    </li>
  );
};

export default TodoItem;
